#include "labs/fiber_scheduler.h"

//
// stackptrs:      Type: addr_t[stackptrs_size].  array of stack pointers (generalizing: main_stack and f_stack)
// stackptrs_size: number of elements in 'stacks'.
//
// arrays:      Type: uint8_t [arrays_size]. array of memory region for stacks (generalizing: f_array)
// arrays_size: size of 'arrays'. equal to stackptrs_size*STACK_SIZE.
//
// Tip: divide arrays into stackptrs_size parts.
// Tip: you may implement a circular buffer using arrays inside shellstate_t
//      if you choose linked lists, create linked linked using arrays in
//      shellstate_t. (use array indexes as next pointers)
// Note: malloc/new is not available at this point.
//

void messi(addr_t* pmain_stack, addr_t* pf_stack, int* pret, bool* pdone, int* n){
	 addr_t& main_stack= *pmain_stack;
	 addr_t& f_stack = *pf_stack;
	 int& ret= *pret;
	 int& nat = *n;
	 bool& notdone= *pdone;
         int i;
         
         
    int counter = 0;
    
    notdone=1;
    if(nat <=0){
		ret = 1;
	}
    else if(nat == 1) {ret= 2;
		}else {
			
			i=3;
    while(true)
    {
        if(isPrime(i))
        {
            counter++;
            notdone=1;
            if(counter == (nat-1)){
             ret= i;
             notdone=0;
             counter=0;
             break;
            }
           
        }
        i++;
        stack_saverestore(f_stack, main_stack);
    }
  }


    notdone=0; stack_saverestore(f_stack,main_stack);

   
}

void neymar(addr_t* pmain_stack, addr_t* pf_stack, int* pret, bool* pdone, int* n){
	 addr_t& main_stack= *pmain_stack;
	 addr_t& f_stack = *pf_stack;
	 int& ret= *pret;
	 int& nat = *n;
	 bool& notdone= *pdone;
         int i;
         
         
    int counter = 0;
    
    notdone=1;
    if(nat <=0){
		ret = 1;
	}
    else if(nat == 1) {ret= 2;
		}else {
			
			i=3;
    while(true)
    {
        if(isPrime(i))
        {
            counter++;
            notdone=1;
            if(counter == (nat-1)){
             ret= i;
             notdone=0;
             counter=0;
             break;
            }
           
        }
        i++;
        
        stack_saverestore(f_stack, main_stack);
    }
  }


    notdone=0; stack_saverestore(f_stack,main_stack);
    
   
}


void shell_step_fiber_scheduler(shellstate_t& shellstate, addr_t main_stack, preempt_t& preempt, addr_t stackptrs[], size_t stackptrs_size, addr_t arrays, size_t arrays_size, dev_lapic_t& lapic){
    
    int& pointer = shellstate.fiberspointer;
	int& argm = shellstate.args[pointer];
	bool& notdone = shellstate.fibers[pointer];
	int& ret = shellstate.rets[pointer]; 
    
    
    
    if(shellstate.fiber_scheduler_state[pointer]==1){
		
        shellstate.fiber_scheduler_state[pointer] = 2;
        
        
        
		if(shellstate.nm[pointer]==1){
			
			stack_init5(stackptrs[pointer], (arrays+(pointer*(arrays_size/5))), arrays_size/5, &neymar, &main_stack, &stackptrs[pointer], &ret, &notdone, &argm);
			
		}else if(shellstate.nm[pointer]==2){
			
			
			stack_init5(stackptrs[pointer], (arrays+(pointer*(arrays_size/5))), arrays_size/5, &messi, &main_stack, &stackptrs[pointer], &ret, &notdone, &argm);
			
		}
		
		
    }else if(shellstate.fiber_scheduler_state[pointer]==2){
		
    
		if(shellstate.fibers[pointer]==1){
			
			stack_saverestore(main_stack, stackptrs[pointer]);
		}else { 
	       
	       //hoh_debug(shellstate.rets[pointer]);
	       
		   tostring(shellstate.result,ret);
			
		   shellstate.fiber_scheduler_state[pointer]=3;
           shellstate.rets[pointer]=0;
           shellstate.fibers[pointer]=0;
           shellstate.toprint = pointer;
   
		}
    }
    

			shellstate.fiberspointer = (shellstate.fiberspointer +1)%5;
	
    
    
}
