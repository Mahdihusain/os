#include "labs/coroutine.h"
#include "labs/vgatext.h"


void shell_step_coroutine(shellstate_t& shellstate, coroutine_t& f_coro, f_t& f_locals){

    coroutine_t& p_coro= f_coro;  
      
	if (shellstate.coroutine_state==1){
		shellstate.coroutine_state=2;
		coroutine_reset(p_coro);
	}
	
	if (shellstate.coroutine_state==2){
		int ret= 0;
		//bool& done= shellstate.f_done;
		int& x= f_locals.x;
		int& y= f_locals.y;

		int& n= shellstate.arg;
		h_begin(p_coro); 
		  
		if(n == 1)  { ret=2;}
		else {
			y=3;
		while(true)
		 {  
			if(isPrime(y))
			{               
				x++;            
				if(x == (n-1)){
				ret=y;
				break;}
			}  
			y++;      
			 h_yield(p_coro);
		 }
		}   
	      
	      tostring(shellstate.result,ret);
		  shellstate.coroutine_state=3;
		  
		  
		  x=0; y=0;
		  h_end(p_coro);
		 }
     
     
}







