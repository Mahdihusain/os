#pragma once

//
// INVARIANT: w_deleted_count <= w_deleting_count <= w_cached_read_count <= shared_read_count <= r_reading_count <= r_cached_write_count <= shared_write_count <= w_writing_count <= w_deleted_count + MAX_SIZE
//
// INVARIANT:      w_writing_count      - w_deleted_count     <= MAX_SIZE
// =========>      w_writing_count      - w_cached_read_count <= MAX_SIZE
// =========>      shared_write_count   - w_cached_read_count <= MAX_SIZE
// =========>      shared_write_count   - shared_read_count   <= MAX_SIZE
//
//
// INVARIANT: 0 <= r_cached_write_count - r_reading_count
// =========> 0 <= r_cached_write_count - shared_read_count
// =========> 0 <= shared_write_count   - shared_read_count
//
//
// THEOREM: =========> 0 <= shared_write_count   - shared_read_count   <= MAX_SIZE
//





//
//
// Channel/Queue:
//
// Shared between Producer and Consumer
//
struct channel_t{
  public:

    size_t read_ptr;
    size_t write_ptr;
    size_t delete_ptr;

  public:

    //
    // Intialize
    //
    channel_t(){

        read_ptr = 0;
        write_ptr = 0;
        delete_ptr = 0;
    }
};


//
// Producer's (not shared)
//
struct writeport_t{
public:
    size_t read_ptr;
    size_t write_ptr;
    size_t delete_ptr;
    size_t qsize;

public:

  //
  // Intialize
  //
  writeport_t(size_t tsize)
  {
    read_ptr = 0;
    write_ptr = 0;
    delete_ptr = 0; 
    qsize = tsize-1;
  }

public:

  //
  // no of entries available to write
  //
  // helper function for write_canreserve
  //
  size_t write_reservesize(){

    if(delete_ptr<=write_ptr){
		
		return qsize + delete_ptr - write_ptr;
		
	}
    
    return delete_ptr - write_ptr;

  }

  //
  // Can write 'n' entries?
  //
  bool write_canreserve(size_t n){

    if(n<=write_reservesize()){
		return true;
	}

    return false;
  }

  //
  // Reserve 'n' entries for write
  //
  size_t write_reserve(size_t n){
    
    size_t temp = write_ptr;
    write_ptr = (write_ptr + n);

    return temp;
  }

  //
  // Commit
  //
  // Read/Write shared memory data structure
  //
  void write_release(channel_t& ch){

    ch.write_ptr = write_ptr;
    //ch.read_ptr = read_ptr;
    //ch.delete_ptr = delete_ptr;

  }




public:

  //
  //
  // Read/Write shared memory data structure
  //
  void read_acquire(channel_t& ch){

    //write_ptr = ch.write_ptr;
    read_ptr = ch.read_ptr;
    //delete_ptr = ch.delete_ptr;

  }




  //
  // No of entires available to delete
  //
  size_t delete_reservesize(){
    
    if(delete_ptr>read_ptr){
		
		return qsize - delete_ptr + read_ptr;
		
	}
    
    return read_ptr-delete_ptr;
    
  }

  //
  // Can delete 'n' entires?
  //
  bool delete_canreserve(size_t n){
    
    if(n<=delete_reservesize()){
		
		return true;
		
	}

    return false;
  }

  //
  // Reserve 'n' entires for deletion
  //
  size_t delete_reserve(size_t n){
    
    size_t temp = delete_ptr;
    delete_ptr = (delete_ptr + n);
    return temp;
    
  }


  //
  // Update the state, if any.
  //
  void delete_release(){
    
    

  }


};


//
// Consumer's (not shared)
//
//
struct readport_t{
public:

  size_t read_ptr;
  size_t write_ptr;
  size_t delete_ptr;
  size_t qsize;

public:
  //
  // Initialize
  //
  readport_t(size_t tsize)
  {

    read_ptr = 0;
    write_ptr = 0;
    delete_ptr = 0; 
    qsize = tsize-1;
     
  }
  public:

  //
  // Read/Write shared memory data structure
  //
  void write_acquire(channel_t& ch){

    write_ptr = ch.write_ptr;
    //read_ptr = ch.read_ptr;
    //delete_ptr = ch.delete_ptr;

  }

  //
  // no of entries available to read
  //
  size_t read_reservesize(){

    if(write_ptr<read_ptr){
		
		return qsize +write_ptr - read_ptr;
		
	}
    
    return write_ptr - read_ptr;
    
  }

  //
  // Can Read 'n' entires?
  //
  bool read_canreserve(size_t n){

    if(n<=read_reservesize()){
		return true;
	}

    return false;
  }

  //
  // Reserve 'n' entires to be read
  //
  size_t read_reserve(size_t n){

    size_t temp = read_ptr;
    read_ptr = (read_ptr + n);
    return temp;
    
  }

  //
  // Read/write shared memory data structure
  //
  void read_release(channel_t& ch){

    //write_ptr = ch.write_ptr;
    ch.read_ptr = read_ptr;
    //delete_ptr = ch.delete_ptr;

  }

};


