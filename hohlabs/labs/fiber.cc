#include "labs/fiber.h"
#include "labs/vgatext.h"

void returnNPrime(addr_t* pmain_stack, addr_t* pf_stack, int* pret, bool* pdone, int* n, preempt_t& preempt){
	 
	 hoh_debug("i have reached");
	 preempt.flag=0;
	 addr_t& main_stack= *pmain_stack;
	 addr_t& f_stack = *pf_stack;
	 int& ret= *pret;
	 int& nat = *n;
	 bool& done= *pdone;
         int i;
    
    //hoh_debug("hello");
    
    int counter = 0;
    
    done=false;
    if(nat <=0){
		ret = 1;
	}
    else if(nat == 1) {ret= 2;
		}else {
			
			i=3;
    while(true)
    {    
		
		hoh_debug("hello");
        if(isPrime(i))
        {
            counter++;
            done=false;
            if(counter == (nat-1)){
             ret= i;
             done=true;
             counter=0;
             break;
            }
           
        }
        i++;
        //stack_saverestore(f_stack, main_stack);
    }
  }


    done=true; //stack_saverestore(f_stack,main_stack);
     
   
}


void shell_step_fiber(shellstate_t& shellstate, addr_t& main_stack, preempt_t& preempt, addr_t& f_stack, addr_t f_array, uint32_t f_arraysize, 
 dev_lapic_t& lapic){
   
    
   addr_t& pmain_stack= main_stack;
   addr_t& pf_stack = f_stack;
   int& ret= shellstate.f_ret;
   bool& done= shellstate.f_done; 
   int& argm= shellstate.arg;
   
   
   if (shellstate.fiber_state==1){     
	   
        shellstate.fiber_state=2; 
        
        stack_init6(f_stack, f_array,f_arraysize, &returnNPrime, &main_stack, &f_stack, &ret, &done, &argm, &preempt);
        
        
   } else if (shellstate.fiber_state==2){
         
         
         if (!done){
			 
			 hoh_debug("imgoing");
			 lapic.reset_timer_count(100000);
             stack_saverestore(pmain_stack, pf_stack);
             hoh_debug("imback");
         }   
         else { 
         
        
        //hoh_debug("till here");
        //hoh_debug(shellstate.f_ret);
       
       tostring(shellstate.result,shellstate.f_ret);
        

      shellstate.fiber_state=3;
      shellstate.f_ret=0; 
      done=false;
      }
      
     
      
   }
   
   //hoh_debug("mainstackcontrols");
 
}

