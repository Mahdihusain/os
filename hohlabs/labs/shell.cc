#include "labs/shell.h"
#include "labs/vgatext.h"

void tostring(char str[], int num);
char tochar(uint8_t scankey);
void fillchar(char chr[]);
int getArg(char str[]);   //int argument
bool isFactorial(char str[]);
int factorial(int n);
int fibonacci(int n);
bool isFibonacci(char str[]);
bool isEcho(char str[]);
bool isCoroutine(char str[]);
bool isFiber(char str[]);
void getstringArg(char str[], char command[]);
bool isNeymar(char str[]);
bool isMessi(char str[]);
int nextFree(bool fibers[]);
//
// initialize shellstate
//
void shell_init(shellstate_t& state){
	state.numKeys=0;
	state.cursor=160;
	state.lastpressed=(uint8_t)0x1c;
	state.lastloc=159;
	fillchar(state.command);
	state.arg=0;
	state.coroutine_state=0;
	state.fiber_state=0;
	state.f_ret=0;
    state.fiberspointer=0;
    state.toprint=0;
    for(int i=0; i<5; i++){
		state.fibers[i] = 0;
		state.nm[i]=0;
	}
	for(int i=0; i<5; i++){
		state.fiber_scheduler_state[i] = 0;
	}
}



void shell_update(uint8_t scankey, shellstate_t& stateinout){
	
	
	
	stateinout.numKeys++;
	if(stateinout.coroutine_state==3){
		stateinout.coroutine_state=0;
	}
	
	if(stateinout.fiber_state==3){
		stateinout.fiber_state=0;
	}
	
	for(int i=0; i<5; i++){
		if(stateinout.fiber_scheduler_state[i]==3){
			stateinout.fiber_scheduler_state[i]=0;
		}		
	}
	
	stateinout.result[0] = '\0'; 
	
	if((scankey)== (uint8_t)0x1c){
		stateinout.lastloc = stateinout.cursor;
		stateinout.cursor = ((stateinout.cursor/80)+2)*80;
		stateinout.lastpressed = (uint8_t)0x1c;
		
	}else if((scankey)== (uint8_t)0x0e){
		stateinout.lastloc = stateinout.cursor;
		if(stateinout.cursor%80!=0){
			stateinout.cursor--;
		}
		stateinout.lastpressed = (uint8_t)0x0e;
	}else{
		stateinout.lastloc = stateinout.cursor;
		stateinout.cursor++;
		stateinout.lastpressed = scankey;
		stateinout.command[stateinout.lastloc%80] = tochar(scankey);
	}
	hoh_debug(stateinout.command);
    hoh_debug("Got: "<<unsigned(scankey));
}


//
// do computation
//
void shell_step(shellstate_t& stateinout){
	
	
	if(stateinout.lastpressed==(uint8_t)0x1c){
		
		
		if(isFactorial(stateinout.command)){
            stateinout.arg= getArg(stateinout.command);
			tostring(stateinout.result, factorial(getArg(stateinout.command)));
			//hoh_debug(stateinout.result);
		}else if(isFibonacci(stateinout.command)){
            stateinout.arg= getArg(stateinout.command);
			tostring(stateinout.result, fibonacci(getArg(stateinout.command)));
			//hoh_debug(stateinout.result);
		}else if(isEcho(stateinout.command)){
            stateinout.arg= getArg(stateinout.command);     
			getstringArg(stateinout.result, stateinout.command);
			//hoh_debug(stateinout.result);
		}else if (isCoroutine(stateinout.command)){
            stateinout.arg= getArg(stateinout.command);            
            stateinout.coroutine_state=1;
            stateinout.corloc = stateinout.cursor;
		}else if (isFiber(stateinout.command)){
            stateinout.arg= getArg(stateinout.command);           
            stateinout.fiber_state=1;
            stateinout.corloc = stateinout.cursor;
            //hoh_debug(stateinout.arg);
		}else if(isMessi(stateinout.command)){
			
			int pointer = nextFree(stateinout.fibers);
			if(pointer==-1){
				fillchar(stateinout.result);
			}else{
				stateinout.args[pointer]= getArg(stateinout.command);
				stateinout.fibers[pointer]=1; 
				stateinout.fiber_scheduler_state[pointer] = 1;
				stateinout.nm[pointer] = 2;				
			}

            stateinout.fiberloc[pointer] = stateinout.cursor;
            
		}else if(isNeymar(stateinout.command)){
			int pointer = nextFree(stateinout.fibers);
			if(pointer==-1){
				fillchar(stateinout.result);
			}else{
				stateinout.args[pointer]= getArg(stateinout.command);
				stateinout.fibers[pointer]=1; 
				stateinout.fiber_scheduler_state[pointer] = 1;
				stateinout.nm[pointer] = 1;				
			}

			stateinout.fiberloc[pointer] = stateinout.cursor;
			
		} 
        else{
			//error(stateinout.result);
		    
		}
		fillchar(stateinout.command);

	}
	
}


//
// shellstate --> renderstate
//
void shell_render(const shellstate_t& shell, renderstate_t& render){
	
	render.numKeys = shell.numKeys;
	render.cursor = shell.cursor;
	render.lastpressed = shell.lastpressed;
	render.lastloc = shell.lastloc;
	render.coroutine_state = shell.coroutine_state;
	render.fiber_state = shell.fiber_state;
	render.corloc = shell.corloc;
    render.toprint = shell.toprint;
	
	for(int i=0; i<5; i++){
		render.fiber_scheduler_state[i] = shell.fiber_scheduler_state[i];
		render.fiberloc[i] = shell.fiberloc[i];
	}
	
	for(int i=0; i<80; i++){
		//hoh_debug(shell.result[i]);
		render.result[i] = shell.result[i]; 
	}
	

}


//
// compare a and b
//
bool render_eq(const renderstate_t& a, const renderstate_t& b){
	
	bool fs = true;
	
	for(int i=0; i<5; i++){
		if(a.fiber_scheduler_state[i]!=b.fiber_scheduler_state[i]){
			fs=false;
			break;
		}
	}
	
	bool res = true;
	
	for(int i=0; i<80; i++){
		
		if(a.result[i]!=b.result[i]){
			res=false;
			break;
		}
		
	}
	
	return (a.numKeys==b.numKeys) && (a.coroutine_state==b.coroutine_state) && (a.fiber_state==b.fiber_state) && fs && res;
	
}


//
// Given a render state, we need to write it into vgatext buffer
//
void render(const renderstate_t& state, int w, int h, addr_t vgatext_base){
	
	int ENOUGH = 10;
	
	char str[ENOUGH];
    tostring(str, state.numKeys);
	
	int loc = 80;
	
	for(int i=0; i<ENOUGH; i++){
		
		if(str[i]=='\0'){
			break;
		}
		
		vgatext::writechar(loc, str[i], 0x7, 0x1, vgatext_base);
		loc += 1;
	}
	
	if(state.coroutine_state==3){
		//tostring(state.result,state.c_ret);
		
		loc = state.corloc-80;
		for(int i=0; i<80; i++){
			
			if(state.result[i]=='\0'){
				break;
			}
			
			vgatext::writechar(loc, state.result[i], 0x7, 0x1, addr_t(0xb8000));
			loc += 1;
		}
		return;
		//state.coroutine_state=0;
	}
	
	if(state.fiber_state==3){
		//tostring(state.result,state.c_ret);
		
		loc = state.corloc-80;
		for(int i=0; i<80; i++){
			
			if(state.result[i]=='\0'){
				break;
			}
			
			vgatext::writechar(loc, state.result[i], 0x7, 0x1, addr_t(0xb8000));
			loc += 1;
		}
		return;
		//state.coroutine_state=0;
	}

		
	if(state.fiber_scheduler_state[state.toprint]==3){
		//tostring(state.result,state.c_ret);
		
		
		
		loc = state.fiberloc[state.toprint]-80;
        
        //hoh_debug(loc);
		
		for(int i=0; i<80; i++){
			
			if(state.result[i]=='\0'){
				break;
			}
			
			vgatext::writechar(loc, state.result[i], 0x7, 0x1, addr_t(0xb8000));
			loc += 1;
		}
		return;
		//state.coroutine_state=0;
	}		

	
	if(state.lastpressed==(uint8_t)0x1c){
	    
	    
	    
		loc = state.cursor-80;
		for(int i=0; i<80; i++){
			
			hoh_debug(state.result[i]);
			
			if(state.result[i]=='\0'){
				break;
				//continue;
			}
			
			vgatext::writechar(loc, state.result[i], 0x7, 0x1, vgatext_base);
			loc += 1;
		}		
		
		vgatext::writechar(state.lastloc,' ',0x0, 0x0, vgatext_base);
	    vgatext::writechar(state.cursor,'_',0x0, 0x7, vgatext_base);

	}else if(state.lastpressed==(uint8_t)0x0e){
		vgatext::writechar(state.lastloc,' ',0x0, 0x0, vgatext_base);
		vgatext::writechar(state.cursor,'_',0x0, 0x7, vgatext_base);
	}else{
		vgatext::writechar(state.lastloc,tochar(state.lastpressed),0x0, 0x7, vgatext_base);
	    vgatext::writechar(state.cursor,'_',0x0, 0x7, vgatext_base);
	}
	

  

}


void tostring(char str[], int num){
    int i, rem, len = 0, n;
    n = num;
    while (n != 0){
        len++;
        n /= 10;
    }
    for (i = 0; i < len; i++){
        rem = num % 10;
        num = num / 10;
        str[len - (i + 1)] = rem + '0';
    }
    str[len] = '\0';
}

char tochar(uint8_t scankey){
     if (scankey==(uint8_t)0x02){return '1';} 
else if (scankey==(uint8_t)0x03 ){return '2';}
else if (scankey==(uint8_t)0x04 ){return '3';}
else if (scankey==(uint8_t)0x05 ){return '4';}
else if (scankey==(uint8_t)0x06 ){return '5';}
else if (scankey==(uint8_t)0x07 ){return '6';}
else if (scankey==(uint8_t)0x08 ){return '7';}
else if (scankey==(uint8_t)0x09 ){return '8';}
else if (scankey==(uint8_t)0x0a ){return '9';}
else if (scankey==(uint8_t)0x0b ){return '0';}
else if (scankey==(uint8_t)0x0c ){return '-';}
else if (scankey==(uint8_t)0x0d ){return '=';}
else if (scankey==(uint8_t)0x10 ){return 'q';}
else if (scankey==(uint8_t)0x11 ){return 'w';}
else if (scankey==(uint8_t)0x12 ){return 'e';}
else if (scankey==(uint8_t)0x13 ){return 'r';}
else if (scankey==(uint8_t)0x14 ){return 't';}
else if (scankey==(uint8_t)0x15 ){return 'y';}
else if (scankey==(uint8_t)0x16 ){return 'u';}
else if (scankey==(uint8_t)0x17 ){return 'i';}
else if (scankey==(uint8_t)0x18 ){return 'o';}
else if (scankey==(uint8_t)0x19 ){return 'p';}
else if (scankey==(uint8_t)0x1a ){return '[';}
else if (scankey==(uint8_t)0x1b ){return ']';}
else if (scankey==(uint8_t)0x1e ){return 'a';}
else if (scankey==(uint8_t)0x1f ){return 's';}
else if (scankey==(uint8_t)0x20 ){return 'd';}
else if (scankey==(uint8_t)0x21 ){return 'f';}
else if (scankey==(uint8_t)0x22 ){return 'g';}
else if (scankey==(uint8_t)0x23 ){return 'h';}
else if (scankey==(uint8_t)0x24 ){return 'j';}
else if (scankey==(uint8_t)0x25 ){return 'k';}
else if (scankey==(uint8_t)0x26 ){return 'l';}
else if (scankey==(uint8_t)0x27 ){return ';';}
else if (scankey==(uint8_t)0x2c ){return 'z';}
else if (scankey==(uint8_t)0x2d ){return 'x';}
else if (scankey==(uint8_t)0x2e ){return 'c';}
else if (scankey==(uint8_t)0x2f ){return 'v';}
else if (scankey==(uint8_t)0x30 ){return 'b';}
else if (scankey==(uint8_t)0x31 ){return 'n';}
else if (scankey==(uint8_t)0x32 ){return 'm';}
else if (scankey==(uint8_t)0x33 ){return ',';}
else if (scankey==(uint8_t)0x34 ){return '.';}
else if (scankey==(uint8_t)0x35 ){return '/';}
else if (scankey==(uint8_t)0x39 ){return ' ';}

}

void fillchar(char chr[]){
	for(int i=0; i<80; i++){
		chr[i]=' ';
	}
}


bool isFactorial(char str[]){
if (str[0]=='f' && str[1]=='a' && str[2]=='c' && str[3]=='t' && str[4]=='o' && str[5]=='r' && str[6]=='i' && str[7]=='a'
    && str[8]=='l') 
   {return true;}
else 
   {return false;}
}

bool isFibonacci(char str[]){
if (str[0]=='f' && str[1]=='i' && str[2]=='b' && str[3]=='o' && str[4]=='n' && str[5]=='a' && str[6]=='c' && str[7]=='c'
    && str[8]=='i') 
   {return true;}
else 
   {return false;}
}

bool isNeymar(char str[]){
if (str[0]=='n' && str[1]=='e' && str[2]=='y' && str[3]=='m' && str[4]=='a' && str[5]=='r') 
   {return true;}
else 
   {return false;}
}

bool isMessi(char str[]){
if (str[0]=='m' && str[1]=='e' && str[2]=='s' && str[3]=='s' && str[4]=='i') 
   {return true;}
else 
   {return false;}
}

bool isEcho(char str[]){
if (str[0]=='e' && str[1]=='c' && str[2]=='h' && str[3]=='o')
   {return true;}
else 
   {return false;}
}

bool isCoroutine(char str[]){
if (str[0]=='c' && str[1]=='o' && str[2]=='r' && str[3]=='o' && str[4]=='u' && str[5]=='t' && str[6]=='i' && str[7]=='n'
    && str[8]=='e') 
   {return true;}
else 
   {return false;}
}

bool isFiber(char str[]){
if (str[0]=='f' && str[1]=='i' && str[2]=='b' && str[3]=='e' && str[4]=='r') 
   {return true;}
else 
   {return false;}
}


void getstringArg(char str[], char command[]){
	
	int start = -1;
	for(int i=0; i<80; i++){
		
		if(start==-1){
			if(command[i]==' '){
				start=0;
			}
		}else{
			if(command[i]==' '){
				str[start] = '\0';
				break;
			}else{
			
				str[start]=command[i];
				start++;
				
		    }

		}
		
	}
	
}

int getArg(char str[]){
	
	int res = -1;
	int start = 0;
	
	for(int i=0; i<80; i++){
		if(start==0){
			if(str[i]==' '){
				start=1;
			}
		}else{
			if(str[i]==' '){
				break;
			}else{
				if(res==-1){
					res=str[i]-'0';
				}else{
					res = res*10 + (str[i]-'0');
				}
			}
		}
	}
	
	return res;
}

int factorial(int n){
   if (n<=1) 
      return 1;
   else 
      return n*factorial(n-1);
} 

int fibonacci(int n){
   if (n==0)
      return 0;
   else if (n==1)
      return 1;
   else
      return (fibonacci(n-1)+fibonacci(n-2));
}


int isPrime(int number)
{
	if(number<=1){
		return 0;
	}
	
    int i;
    for (i=2; i*i<=number; i++) {
        if (number % i == 0) return 0;
    }
    return 1;
}

int nextFree( bool fibers[]){
	
	int counter=0;
	int i=0;
	
	while(true){
		if(fibers[i]==0){
			return i;
		}else{
			i = (i+1)%5;
		}
		counter++;
		if(counter==5){
			return -1;
		}
	}
	
}

//
// handle keyboard event.
// key is in scancode format.
// For ex:
// scancode for following keys are:
//
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//      | keys     | esc |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 |  0 |  - |  = |back|
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//      | scancode | 01  | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 0a | 0b | 0c | 0d | 0e |
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//      | keys     | tab |  q |  w |  e |  r |  t |  y |  u |  i |  o |  p |  [ |  ] |entr|
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//      | scancode | 0f  | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 1a | 1b | 1c |
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//      | keys     |ctrl |  a |  s |  d |  f |  g |  h |  j |  k |  l |  ; |  ' |    |shft|
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//      | scancode | 1d  | 1e | 1f | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 2a |
//      +----------+-----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//

// so and so..
//
// - restrict yourself to: 0-9, a-z, esc, enter, arrows
// - ignore other keys like shift, control keys
// - only handle the keys which you're interested in
// - for example, you may want to handle up(0x48),down(0x50) arrow keys for menu.
//
