#pragma once
#include "util/config.h"
#include "util/debug.h"

struct shellstate_t{
	int numKeys;
	int cursor;
	int lastloc;
	int corloc;
	uint8_t lastpressed;
	char command[80];
	char result[80];
	int arg;
	int coroutine_state; //3 to render
	int f_ret;
	bool f_done;
	int fiber_state;
	int nm[5];
	bool fibers[5];
	int fiberspointer;
	int args[5];
	int rets[5];
	int fiber_scheduler_state[5];
	int fiberloc[5];
	int toprint;
};

struct renderstate_t{
	int numKeys;
	int cursor;
	int lastloc;
	int corloc;
	uint8_t lastpressed;
	char result[80];
	int coroutine_state;
	int fiber_state;
	int fiber_scheduler_state[5];
	int fiberloc[5];
	int toprint;
};

void shell_init(shellstate_t& state);
void shell_update(uint8_t scankey, shellstate_t& stateinout);
void shell_step(shellstate_t& stateinout);
void shell_render(const shellstate_t& shell, renderstate_t& render);

bool render_eq(const renderstate_t& a, const renderstate_t& b);
void render(const renderstate_t& state, int w, int h, addr_t display_base);

void tostring(char str[], int num);
int isPrime(int number);
